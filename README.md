# undertale yellow saves
### brought to you by bunny <3

## description
collection of undertale yellow saves, wrapped into a html file.
these saves made on version 1.0.0

## instructions
find your desired file
rename to save.sav
place into %userprofile%\appdata\local\undertale yellow\

## roadmap
hope to turn this into a learning experience and make a save editor.

## ack
play undertale yellow [here](https://gamejolt.com/games/UndertaleYellow/136925)

## license
all code licensed under the mit license.
disassembly/reimplementations of game mechanics fall under fair use.